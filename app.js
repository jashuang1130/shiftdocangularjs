var myApp = angular.module('endOfShiftForm',[]);


  myApp.controller('endShiftController',['$scope', function($scope){
      angular.element(document).ready(function() {
        //dropdown initialization
        $('select').material_select();
        // datepicker initialization
        $('.datepicker').pickadate({
          selectMonths: true, // Creates a dropdown to control month
          selectYears: 15 // Creates a dropdown of 15 years to control year
        });
      });
    $scope.mentors = [
      {
        name: "Ben"
      },
      {
        name: "Jack"
      },
      {
        name: "Erin"
      },
      {
        name: "Lynn"
      }
    ];

    $scope.students = [
      {
        name: "Robert",
        issues: {
          mentor: "",
          onetoone:"",
          issues:""
        }
      },
      {
        name: "Kristy",
        issues: {
          mentor: "",
          onetoone:"",
          issues:""
        }
      },
      {
        name: "Jon",
        issues: {
          mentor: "",
          onetoone:"",
          issues:""
        }
      },
      {
        name: "William",
        issues: {
          mentor: "",
          onetoone:"",
          issues:""
        }
      },
    ];

    $scope.studentIssue = '';
    $scope.workbookIssue = '';
    $scope.onetoone= '';
  }]);
